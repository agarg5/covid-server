# Important

Start this server using `npm run server` after doing `npm install`

## Getting Started

Please follow these instructions to get the code running on your local machine

#### Install the Development Dependencies

- Install [git](https://git-scm.com/) for version control on your local computer if you haven't done so already
- Install [Visual Studio](https://visualstudio.microsoft.com/) or use your prefered editor
- Install [Docker](https://www.docker.com/) and [Docker-Compose](https://docs.docker.com/compose/install/)

#### Clone the Repository

- Copy the URL of this repo and clone it to your local machine.
  \$ git clone https://gitlab.com/agarg5/business-sign-on
- cd inside the cloned repo

#### Start the development database

- In a bash environment: `source development/mongo.env` (Loads development settings)
- Get the twilio credentials from agarg5 and add them to your bash environment:

```
export TWILIO_PHONE_NUMBER="<some number>"
export TWILIO_ACCOUNT_SID="<some sid>"
export TWILIO_TOKEN="<some token>"
```

- run `npm install` to install the node dependencies
- run `npm start` to open the application in the browser
  Congrats, You now have the code up and running!

## Terminology

giver: user who volunteers to fufill a need
needer: use who requests help

## Notes on Twilio

We are using [Twilio](https://www.twilio.com/) for message relaying. This means that all incoming texts from the giver get relayed on to the needer and vice versa so that a the needer/giver can be in a text message group with eachother. It was set up this way because Twilio doesn't allow creating a text group of more than one recipient. This text message relaying also for `masking` as in the needer and giver just text us and don't find out eachother's phone numbers. If we get more scale, we can use [Bandwidth](bandwidth.com) instead of Twilio which does allow creating text groups with multiple recipients. We chose not to use bandwith right now because it: 1) requires being a registered business, 2) requires a minimum payment of \$100 a month, a 12 month contract with the first two months as a free and when it is possible to opt out, 3) would make phone number masking more challenging.

## What to get started on

get the code running locally on your computer and play around with it. When you feel like you have a good sense of it, take a look at the issues list at the [issues list](https://gitlab.com/agarg5/business-sign-on/issues). Find one you would like to work on. Assign the issue to yourself and follow the git practices mentioned in this document to work on that issue. Put a completion ETA in the comments of the issue

As you discover other bugs, feature suggestions, please add them to this issue list along with:

- a detailed description,
- gifs + images showing the solution
- the testing plan as well as passing tests if applicable
- challenges encountered
- any code debt created
- the approach taken to solving the problem

## Git Practices

For many of the tasks, the code will contain a comment with the task URL to direct to the relevant section. For example if working on this [issue4](https://gitlab.com/agarg5/covid-server/-/issues/4) search the task URL(https://gitlab.com/agarg5/covid-server/-/issues/4) in code to find a comment indicating where you will likely have to write the code for this task.

When developing code, please the feature in a new branch. Name this branch something meaningful and include the number of the task that it is solving (for example `23-fix/hover-on-like-button).

Make a merge request after the first commit and include `WIP` in the title as well as attaching a `WIP` label. Doing this helps track progress. Remember to commit early and often, push frequently, and merge/rebase master frequently into your branch to avoid merge conflicts.

When ready to submit the merge request, remember to test in Chrome, Firefox, Safari, IE as well as on mobile. Include a few screenshots or preferably gifs in the description setion of the merge request. Use a software like Gifox to make gifs. Finally, remove the `WIP` from the title and its label and notify the engineering chanel that your code is ready to review.

Please follow the naming convention from this [git style reference](https://github.com/agis/git-style-guide)

## Code Best Practices

- Don't use magic numbers, use const variables defined at top of files instead. const variables should have const prefix and variable names should be in snake case in all caps

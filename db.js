const MongoClient = require("mongodb").MongoClient;

const dbConnectionUrl = `mongodb+srv://${process.env.USERNAME}:${process.env.PASSWORD}@cluster1-lbcjy.mongodb.net/test?retryWrites=true&w=majority`;

const initialize = (dbName, dbCollectionName) =>
  new Promise((resolve, reject) =>
    MongoClient.connect(dbConnectionUrl, (err, dbInstance) => {
      if (err) {
        console.log(`[MongoDB connection] ERROR: ${err}`);
        reject(err); // this should be "caught" by the calling function
      } else {
        const dbObject = dbInstance.db(dbName);
        const dbCollection = dbObject.collection(dbCollectionName);
        resolve(dbCollection);
      }
    })
  );

module.exports = {
  initialize
};

import cors from "cors";
import get from "lodash.get";
import twilio from "twilio";
const MessagingResponse = require("twilio").twiml.MessagingResponse;
import convert from "convert-units";
import { ObjectID } from "mongodb";
import bodyParser from "body-parser";
import express from "express";
import dotenv from "dotenv";

dotenv.config();

const app = express();

app.use(cors());

const PORT = process.env.PORT || 9000;

const DEFAULT_DISTANCE_TOLERANCE_MILES = 250000000000; // TODO change to 25

app.listen(PORT, () => console.log(`Example app listening on port ${PORT}!`));

const NO_RESPONSE_TEXT =
  "We are trying to find someone who can help you out soon. In the meantime, please just hang tight";

const NEED_CREATED_TEXT =
  "Thanks for submitting your need. We will message you when someone is available to help";

app.get("/", (req, res) => {
  res.send("Hello World!");
});

const db = require("./db");
const dbName = "data";
const collectionName = "data";

app.use(bodyParser());

app.use(bodyParser.urlencoded({ extended: true }));

db.initialize(dbName, collectionName).then(async dbCollection => {
  dbCollection.createIndex({ "location.coordinates": "2d" });
  dbCollection.createIndex({
    giverPhoneNumber: "text",
    neederPhoneNumber: "text"
  });
});

const addNeed = async (need, location) => {
  const dbCollection = await db.initialize(dbName, collectionName);
  dbCollection.insertOne(
    {
      ...need,
      location: { type: "Point", coordinates: location }
    },
    (error, result) => {
      if (error) throw error;
      console.log(result);
    }
  );
};

const getNeedsCloseBy = async (
  lat,
  long,
  distance = DEFAULT_DISTANCE_TOLERANCE_MILES
) => {
  const dbCollection = await db.initialize(dbName, collectionName);
  const distanceTolerance = convert(distance)
    .from("mi")
    .to("m");

  const results = await dbCollection
    .find({
      location: {
        $near: {
          $geometry: { type: "Point", coordinates: [long, lat] },
          $minDistance: 0,
          $maxDistance: distanceTolerance
        }
      },
      fufilling: { $exists: false },
      completed: { $exists: false }
    })
    .toArray();
  return results;
};

// TODO https://gitlab.com/agarg5/covid-server/-/issues/4 make sure that neederPhoneNumber not used present in another incomplete need. return HTTP error if so
app.post("/add-need", async function(req, res) {
  const { location, need } = get(req, "body", {});
  const { latitude, longitude } = location;
  const { neederPhoneNumber } = need;
  await addNeed(need, [longitude, latitude]);
  sendText(NEED_CREATED_TEXT, neederPhoneNumber);
  res.send(200);
});

app.get("/get-needs", async function(req, res) {
  const { latitude, longitude, distance } = get(req, "query", {});
  if (!latitude || !longitude)
    return res.status(500).send("Please supply latitutde and longitude");

  return res.json(
    await getNeedsCloseBy(parseFloat(latitude), parseFloat(longitude), distance)
  );
});

// puts need in database and texts needer and giver to put them in touch
// TODO https://gitlab.com/agarg5/covid-server/-/issues/4 make sure that giverPhoneNumber not used present in another incomplete need. return HTTP error if so
app.get("/fufill-need", async function(req, res) {
  const { id, phoneNumber: giverPhoneNumber, name: giverName } = get(
    req,
    "query",
    {}
  );
  if (!id) return res.status(500).send("Please supply a need id");
  const dbCollection = await db.initialize(dbName, collectionName);
  const need = await dbCollection.findOneAndUpdate(
    { _id: new ObjectID(id) },
    {
      $set: {
        started: new Date(),
        giverPhoneNumber,
        giverName,
        completed: false
      }
    }
  );
  const {
    neederPhoneNumber,
    needCategory,
    needDescription,
    name: neederName
  } = need;

  // messages giver
  sendText(
    `Thanks for helping ${neederName} with ${needCategory} (${needDescription}). Respond to this text to message ${neederName}`,
    giverPhoneNumber
  );

  // messages needer
  sendText(
    `${giverName} will fufill your ${needCategory} (${needDescription}). Respond to this text to coordinate on next steps with ${giverName}`,
    neederPhoneNumber
  );
});

const client = new twilio(
  process.env.TWILIO_ACCOUNT_SID,
  process.env.TWILIO_TOKEN
);

const sendText = (text, phoneNumber) =>
  client.messages
    .create({
      body: text,
      to: phoneNumber,
      from: process.env.TWILIO_PHONE_NUMBER
    })
    .catch(error => console.error(error));

// returns needer phone number if giver phone number is supplied and vice versa
// TODO https://gitlab.com/agarg5/covid-server/-/issues/1 we can store the text message content in the database for analysis and moderation
// TODO https://gitlab.com/agarg5/covid-server/-/issues/2 consider making a cache of phone number pairs for preformance. Might to make a big difference because mongo index has already been created
const getPartner = async senderPhoneNumber => {
  const dbCollection = await db.initialize(dbName, collectionName);

  const need =
    (await dbCollection.findOne({
      giverPhoneNumber: senderPhoneNumber,
      completed: false
    })) ||
    (await dbCollection.findOne({
      neederPhoneNumber: senderPhoneNumber,
      completed: false
    }));

  if (!need) return null;

  return need.neederPhoneNumber === senderPhoneNumber
    ? need.giverPhoneNumber
    : senderPhoneNumber;
};

// listens to incoming text messages. relays messages between needer and giver
// TODO https://gitlab.com/agarg5/covid-server/-/issues/3 see if task has been completed
app.post("/sms", async (req, res) => {
  const twiml = new MessagingResponse();
  const { Body: text, From: sender } = get(req, "body");
  const partnerPhoneNumber = await getPartner(sender);
  // phone number for testing. feel free to use this or your own. phone numbers currently have to be verified in twilio to work. Please ask Abhi (agarg5@alumni.stanford.edu) to do so
  // sendText(text, "+15107752174");
  if (partnerPhoneNumber) sendText(text, partnerPhoneNumber);
  else sendText(NO_RESPONSE_TEXT, sender);
});
